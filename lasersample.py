preamble=[
    "G28",
    ";G28"
]
focus_height=30
x_offs=10
y_offs=10
x_block_distance=15
y_block_distance=15
line_template="G1 X{} Y{}"
laser_on="M106 S{}\n"
laser_off="M106 S255\n"
block_with=20
lines=[0,0.125,0.25,0.5,1]
speeds=[100,200,500,1000,1500,2000,2500,3000]
intensities=[10,20,30,40,50,80,70,80,90,100]
current_intensity=0
current_speed=0

def prolog():
    print (";prolog")
    for line in preamble:
        print (line)
    print ("G1 Z{}".format(focus_height))
    print (";endprolog")

def epilog():
    print (";epilog")
    
def calc_laser_intensity(percentage):
    return (255-(255*percentage)/100)

def block_header():
    print (";block header")
    print (";intensity {}".format(current_intensity))
    print (";speed {}".format(current_speed))

def one_block(x=0, y=0):
    for localy in range (0,len(lines)):
        one_line(x,y+lines[localy])

def block_footer():
    print (";block footer")

def row_header():
    print (";row header")
    print (";intensity {}".format(current_intensity))
    print (";speed {}".format(current_speed))

def one_row(x=0,y=0):
    row_header()
    for localx in range(1, len(intensities)):
        global current_intensity
        current_intensity=intensities[localx-1]
        block_header()
        one_block(x+x_block_distance*(localx-1),y)
        block_footer()
    row_footer()

def row_footer():
    print (";row footer")

def one_line(x = 0, y = 0):
    print (line_template.format(x,y))
    print (laser_on.format(current_intensity),end="")
    print (line_template.format(x+block_with,y))
    print (laser_off,end="")
    

prolog()
for y in range(1, len(speeds)):
    current_speed=speeds[y-1]
    print ("F{}".format(current_speed))
    one_row(x_offs,y_offs+(y-1)*y_block_distance)

  